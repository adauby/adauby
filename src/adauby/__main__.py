import logging
import os
from signal import signal, SIGINT

import typer
from inquirer import prompt
from typing_extensions import Annotated
from dependency_injector.wiring import Provide, inject

from adauby.log_readers import LogReader
from adauby.config import Configurations, Parameters
from adauby.containers import Container
from adauby.utils import select_display_format, LogOrigin, select_sleep, sigint_handler

app = typer.Typer()

log_reader: LogReader
existing_params: Parameters
configuration: Configurations


@inject
def inject_dependencies(log_origin: LogOrigin, message_receiver: LogReader = Provide[Container.message_broker]
                        , parameters: Parameters = Provide[Container.params]
                        , configs: Configurations = Provide[Container.configuration]
                        , log_file_reader: LogReader = Provide[Container.log_file_reader]):
    global log_reader
    global existing_params
    global configuration
    if log_origin == LogOrigin.RABBIT:
        log_reader = message_receiver
    else:
        log_reader = log_file_reader
    existing_params = parameters
    configuration = configs


def _init(log_origin: LogOrigin = LogOrigin.RABBIT):
    container = Container()
    container.init_resources()
    container.wire(modules=[__name__])
    inject_dependencies(log_origin)


def configure_keywords(keywords):
    if keywords is not None:
        if keywords.strip() != '':
            configuration.file_path = keywords
            configuration.write_keywords(configuration.read_keywords_from_file())


@app.command("config")
def configure_broker_access(
        set_keywords: Annotated[str, typer.Option("--set-keywords", "-k",
                                                  help='Specify file path containing keywords. '
                                                       '(Ex: "--set-keywords /path/to/keywords.txt" or "-k '
                                                       '/path/to/keywords.txt")')] = '',
        show: Annotated[bool, typer.Option("--show", "-S", help='Show actual configuration')] = False,
        file_only: Annotated[bool, typer.Option("--file-only", "-f",
                                                help='Set configuration for reading file only')] = False):
    """
    Allow to set configuration parameters
    """

    try:
        _init()
        if show:
            configuration.show()
        else:
            if file_only:
                configuration.set_config_param_for_file_only()
                configure_keywords(set_keywords)
            else:
                configuration.set_config_params_from_user_input()
                configure_keywords(set_keywords)

            configuration.write_config()

    except Exception as e:
        logging.error(e)


@app.command("monitoring")
def display_logs(
        pattern: Annotated[str, typer.Option("--pattern", "-p",
                                             help='Pattern to search in logs. (Ex: "--pattern word" or "-p word")')] = None,
        save_to: Annotated[str, typer.Option("--save-to-dir", "-D",
                                             help='Directory where to save logs. '
                                                  'To save in the current directory use ".". '
                                                  '(Ex: "--save-to-dir /home/user" or "-D /home/user")')] = None,
        filter_by_keywords: Annotated[bool, typer.Option("--filter-by-keywords", "-k",
                                                         help='Filter logs by key word. The default keywords are: '
                                                              'INFO, WARN, ERROR, DEBUG.')] = False,
        force_display: Annotated[bool, typer.Option("--force-display", "-d",
                                                    help='Force display format: TEXT (default) and JSON')] = False,
        read_file: Annotated[str, typer.Option("--read-file", "-f",
                                               help='Read log from file. (Ex: "--read-file /path/to/file.log" '
                                                    'or "-f /path/to/file.log") ')] = None,
        force_sleep: Annotated[bool, typer.Option("--force-sleep", "-F",
                                                  help='Force sleep time between two lines. [1 second, 0.7 second - '
                                                       '0.5 second (default), 0.3 second, 0.1 second]. ')] = False):
    """
    Monitor log from message broker or file (text or json)
    """
    try:
        _init()
        keywords = None
        display_format = None
        sleep_time = None
        if filter_by_keywords:
            keywords = prompt(configuration.build_keywords_selection_prompt())
        if force_display:
            display_format = select_display_format(force_display)
        if force_sleep:
            sleep_time = select_sleep(force_sleep)
        if read_file is None:
            _init()
            log_reader.read_log(pattern, keywords, save_to, display_format)
        else:
            _init(LogOrigin.FILE)
            log_reader.read_log(pattern, keywords, save_to, display_format, read_file, sleep_time)
    except Exception as ex:
        print(ex)


if __name__ == "__main__":
    app()
