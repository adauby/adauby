import os
import platform
from enum import Enum
import json
import logging
from datetime import datetime
from pathlib import Path
from pprint import pprint

import readchar
from inquirer import List, prompt
from rich import print as rprint

KEY_DATE = 'date'
KEY_LEVEL = 'level'
KEY_LABEL = 'label'
KEYWORDS = 'keywords'
KEY_MESSAGE = 'message'
JSON_INDENT = 2

PLATFORM_WINDOWS: str = 'Windows'
EXCHANGE_FANOUT = 'fanout'
EXCHANGE_DIRECT = 'direct'
EXCHANGE_TOPIC = 'topic'
TYPE = 'type'
MODE = 'mode'
SLEEP = 'sleep'
EMPTY = ""
MUST_BE_A_NUMBER = "([red]must be a number[green bold])"

TXT_LOG_EXTENSION = '.log'
JSON_LOG_EXTENSION = '.json'

EXIST_DEFAULT = 'Existing or default'

MSG_ONE_SECOND = '1 second'
MSG_SEVEN_HUNDRED_MILLI = '0.7 second'
MSG_FIVE_HUNDRED_MILLI = '0.5 second'
MSG_THREE_HUNDRED_MILLI = '0.3 second'
MSG_HUNDRED_MILLI = '0.1 second'

ONE_SECOND = '1'
SEVEN_HUNDRED_MILLI = '0.700'
FIVE_HUNDRED_MILLI = '0.500'
THREE_HUNDRED_MILLI = '0.300'
HUNDRED_MILLI = '0.100'
CONFIG_FOLDER = ".adauby"


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        """
        Possible changes to the value of the `__init__` argument do not affect
        the returned instance.
        """
        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]


class MonitoringSummary(metaclass=Singleton):
    def __init__(self):
        self._both_keywords_and_pattern_match = 0
        self._keywords_match = 0
        self._pattern_match = 0
        self._total_line_read_count: int = 0
        self._number_of_line_matching_filter = 0
        self._origin: str = ""
        self._file_name: str = ""
        self._date = datetime.today()
        self._path_to_summary = None
        self._pattern = ""
        self._keywords = []
        self._summary = None
        self._folder = self._get_summaries_folder()

    def get_total_line_read_count(self):
        return self._total_line_read_count

    def get_number_of_line_matching_filter(self):
        return self._number_of_line_matching_filter

    def increment_total_line_read_count(self):
        self._total_line_read_count += 1

    def increment_number_of_line_matching_filter(self):
        self._number_of_line_matching_filter += 1

    def set_origin_of_monitoring(self, origin: str):
        self._origin = origin

    def set_keywords(self, keywords):
        self._keywords = keywords

    def set_pattern(self, pattern):
        self._pattern = pattern

    def set_file_name(self, file_name):
        self._file_name = file_name

    def increment_pattern_match(self):
        self._pattern_match += 1

    def increment_keywords_match(self):
        self._keywords_match += 1

    def increment_both_keywords_and_pattern_match(self):
        self._both_keywords_and_pattern_match += 1

    def save_summary(self):
        if self._summary is not None:
            self._create_summaries_folder()
            today = datetime.today().strftime('%Y-%m-%d')
            current_platform: str = platform.system()
            summary_file = self._folder + "/summaries_" + today + ".json"
            if current_platform == PLATFORM_WINDOWS:
                summary_file = self._folder + "\\summaries_" + today + ".json"

            with open(summary_file, 'a') as s_file:
                s_file.write(convert_message_to_json(self._summary) + "\n")

    def _create_summaries_folder(self):
        if not os.path.exists(self._folder):
            os.makedirs(self._folder)

    # TODO: consider os
    def _get_summaries_folder(self):
        user_home = str(Path.home())
        return os.path.join(user_home, "adauby/logs/summaries")

    def show_summary(self):
        missed = 0
        if self._keywords is not None or self._pattern is not None:
            missed = self._total_line_read_count - self._number_of_line_matching_filter
        filter_applied = {
            "pattern": self._pattern,
            "keywords": self._keywords,
            "pattern-only match": self._pattern_match,
            "keyword-only match": self._keywords_match,
            "matching both keywords and pattern": self._both_keywords_and_pattern_match,
            "total filtered": self._number_of_line_matching_filter,
            "missed": missed
        }
        self._summary = {"from": self._origin,
                         "file name": self._file_name,
                         "started at": self._date.strftime('%Y-%m-%d %H:%M:%S'),
                         "ended at": datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
                         "total lines read": self._total_line_read_count,
                         "filter": filter_applied
                         }
        rprint("[purple]================================================================================================")
        rprint("[green bold]SUMMARY :")
        print(convert_message_to_json(self._summary, JSON_INDENT))
        rprint("[purple]================================================================================================")


class DisplayFormat(Enum):
    TEXT = 1
    # VALUE = 2
    JSON = 2


class LogOrigin(Enum):
    RABBIT = 1
    FILE = 2


def _from_dic_to_json(json_dic: dict, indent=0):
    if indent == 0:
        return json.dumps(json_dic)
    else:
        return json.dumps(json_dic, indent=indent)


def convert_message_to_json(message, indent: int = 0):
    try:
        json_data = '{}'
        if type(message) is dict:
            json_data = _from_dic_to_json(message, indent)
        elif message is not None and message != EMPTY:
            json_data = convert_json_data_to_dictionary(message)
            json_data = _from_dic_to_json(json_data, indent)
    except ValueError as e:
        logging.error(e)
        json_data = message
    return json_data


def convert_json_data_to_dictionary(message):
    message_dic = dict()
    try:
        if message is not None and message != EMPTY:
            message_dic = json.loads(message)
    except ValueError as e:
        logging.error(e)
        message_dic = message

    return message_dic


def is_valid_json(message):
    is_valid = True
    try:
        json.loads(message)
    except ValueError as e:
        logging.error(e)
        is_valid = False
    return is_valid


def rabbitmq_exchange_type_prompt():
    return [
        List(
            TYPE,
            message='Select a type ',
            choices=[EXIST_DEFAULT, EXCHANGE_FANOUT, EXCHANGE_DIRECT, EXCHANGE_TOPIC],
        )
    ]


def display_format_prompt(forced: bool = False):
    message = 'Select a display format '
    format_list = [EXIST_DEFAULT, DisplayFormat.TEXT.name, DisplayFormat.JSON.name]
    if forced:
        format_list.remove(EXIST_DEFAULT)
        return [
            List(
                MODE,
                message=message,
                choices=format_list,
            )
        ]
    return [
        List(
            MODE,
            message=message,
            choices=format_list,
        )
    ]


def display_speed_prompt(force_sleep):
    message = 'Select time between two lines '
    sleep_list = [EXIST_DEFAULT, MSG_ONE_SECOND, MSG_SEVEN_HUNDRED_MILLI, MSG_FIVE_HUNDRED_MILLI,
                  MSG_THREE_HUNDRED_MILLI,
                  MSG_HUNDRED_MILLI]
    if force_sleep:
        sleep_list.remove(EXIST_DEFAULT)
        return [
            List(
                SLEEP,
                message=message,
                choices=sleep_list,
            )
        ]

    return [
        List(
            SLEEP,
            message=message,
            choices=sleep_list,
        )
    ]


def select_exchange_type():
    exchange_type = prompt(rabbitmq_exchange_type_prompt())
    if exchange_type[TYPE] in [EXCHANGE_FANOUT, EXCHANGE_DIRECT, EXCHANGE_TOPIC]:
        return exchange_type[TYPE]

    return EXCHANGE_DIRECT


def select_display_format(forced: bool = False, existing_config=None):
    display_format = prompt(display_format_prompt(forced))
    match display_format[MODE]:
        case DisplayFormat.TEXT.name:
            return DisplayFormat.TEXT.name.lower()
        case DisplayFormat.JSON.name:
            return DisplayFormat.JSON.name.lower()
        # case DisplayFormat.VALUE.name:
        #     return DisplayFormat.VALUE.name.lower()
        case _:
            if existing_config is None:
                return DisplayFormat.TEXT.name.lower()
            return existing_config


def select_sleep(forced: bool = False, existing_config=None):
    sleep_time = prompt(display_speed_prompt(forced))
    if sleep_time[SLEEP] == MSG_ONE_SECOND:
        return ONE_SECOND
    elif sleep_time[SLEEP] == MSG_SEVEN_HUNDRED_MILLI:
        return SEVEN_HUNDRED_MILLI
    elif sleep_time[SLEEP] == MSG_FIVE_HUNDRED_MILLI:
        return FIVE_HUNDRED_MILLI
    elif sleep_time[SLEEP] == MSG_THREE_HUNDRED_MILLI:
        return THREE_HUNDRED_MILLI
    elif sleep_time[SLEEP] == MSG_HUNDRED_MILLI:
        return HUNDRED_MILLI
    elif sleep_time[SLEEP] == EXIST_DEFAULT:
        if existing_config is None:
            return FIVE_HUNDRED_MILLI
        return existing_config


def print_message(message):
    rprint(message)
    rprint('[white]Type "Enter" to use default or existing configuration[white].')


def show_config_param(label: str, param_value):
    rprint(f"[bright_white]{label}: [green]{param_value}")


def get_value_from_user_input(user_input, param, default_value: int, param_name: str) -> int:
    count: int = 0
    if user_input == EMPTY:
        if param is None:
            return default_value
    else:
        while not user_input.isnumeric() and count == 0:
            rprint(f"[green bold]Enter {param_name} {MUST_BE_A_NUMBER}: ")
            rprint('[white]Type "Enter" to use default or existing port configuration[white].')
            user_input = input()
            count += 1
        if user_input.isnumeric():
            return int(user_input)
        elif param is None:
            rprint(f"[yellow]The default value [red]{default_value} [yellow]will be set.")
            return default_value
    rprint(f"[yellow]{param_name} : [red]{param}.")
    return param


def sigint_handler(signum, frame):
    rprint("\n[bold]Do you really want to terminate the program ? \n[white]Type [red](a/A) [white]or [red](y/Y) ["
           "white]to terminate or [green]any other key to continue")
    char = readchar.readchar()
    if char.lower() in ('y', 'a'):
        summary = MonitoringSummary()
        summary.show_summary()
        summary.save_summary()
        exit(1)


def get_config_path() -> str:
    user_home = str(Path.home())
    current_platform: str = platform.system()
    config_folder = user_home + "/" + CONFIG_FOLDER + "/"
    if current_platform == PLATFORM_WINDOWS:
        config_folder = user_home + "\\" + CONFIG_FOLDER + "\\"
    if not os.path.exists(config_folder):
        os.mkdir(config_folder)

    return config_folder


