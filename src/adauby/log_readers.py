import logging
from signal import signal, SIGINT

from pika import ConnectionParameters, BlockingConnection
from pika.exchange_type import ExchangeType
from pika.credentials import PlainCredentials as Credentials
from rich import print as rprint
from abc import ABC, abstractmethod

from .utils import is_valid_json, DisplayFormat, EMPTY, sigint_handler, MonitoringSummary, LogOrigin
from .log_line import LogDisplay
from .config import Parameters
from time import sleep

_ROUTING_KEY = "log"
CONNECTION_ERROR_MESSAGE = ("Adauby cannot connect to rabbitMQ. Please make sure rabbitMQ instance or your connection "
                            "is up.")


class LogReader(ABC):
    def __init__(self, log_display: LogDisplay,
                 params: Parameters
                 ) -> None:
        self.params = params
        self.log_display = log_display
        self.log_display.display_format = params.get_display_format()

    @abstractmethod
    def read_log(self, pattern: str = None, selected_kwrds: any = None, save_to: str = None,
                 display_format=None, log_file: str = None, sleep_time: float = None):
        pass

    def force_display_format(self, display_format):
        if display_format is not None:
            self.log_display.display_format = display_format


class LogFileReader(LogReader):

    def __init__(self, log_display: LogDisplay, params: Parameters):
        super().__init__(log_display, params)
        self.logger = logging.getLogger(f"{__name__}.{self.__class__.__name__}", )

    def read_log(self, pattern: str = None, selected_kwrds: any = None, save_to: str = None,
                 display_format=None, log_file: str = None, sleep_time: float = None):
        try:
            summary = MonitoringSummary()
            summary.set_origin_of_monitoring(LogOrigin.FILE.name)
            summary.set_file_name(log_file)
            self.force_display_format(display_format)
            if sleep_time is not None:
                self.params.set_sleep(sleep_time)
            if (pattern is None or pattern.strip() == EMPTY) and (selected_kwrds is None or len(selected_kwrds) == 0):
                save_to = None
            with open(log_file) as file:
                for line in file:
                    self.log_display.display_logs(line, pattern=pattern, keywords=selected_kwrds, log_dir=save_to,
                                                  from_file=True)
                    sleep(float(self.params.get_sleep()))

            MonitoringSummary().show_summary()

        except FileNotFoundError as ex:
            self.logger.error(ex)


class RabbitReader(LogReader):
    def __init__(self, log_display: LogDisplay, params: Parameters) -> None:
        super().__init__(log_display, params)
        self.logger = logging.getLogger(f"{__name__}.{self.__class__.__name__}", )
        self.selected_keywords = None
        self.pattern = None
        self.save_to = None

    def connect(self):
        try:
            connection = BlockingConnection(ConnectionParameters(host=self.params.host,
                                                                 port=self.params.port,
                                                                 credentials=Credentials(self.params.user,
                                                                                         self.params.password),
                                                                 virtual_host=self.params.vhost))
            channel = connection.channel()
            channel.exchange_declare(exchange=self.params.exchange, exchange_type=ExchangeType.direct, durable=True)

            channel.queue_declare(queue=self.params.queue_name, durable=True)

            # TODO: parameterize routing_key value

            channel.queue_bind(exchange=self.params.exchange, queue=self.params.queue_name, routing_key=_ROUTING_KEY)
            channel.basic_consume(queue=self.params.queue_name, on_message_callback=self.callback, auto_ack=True)
            rprint(' [yellow]Waiting for logs. To exit press CTRL+C[yellow]')
            channel.start_consuming()

        except Exception as e:
            self.logger.error(CONNECTION_ERROR_MESSAGE)
            self.logger.error(e)
        # except Exception as e:
        #     logging.error("An unexpected error occurs. Adauby cannot connect to rabbitMQ instance!")

    def read_log(self, pattern: str = None, selected_kwrds: any = None, save_to: str = None,
                 display_format=None, log_file: str = None, sleep_time: float = None):
        MonitoringSummary().set_origin_of_monitoring(LogOrigin.RABBIT.name)
        self.pattern = pattern
        self.selected_keywords = selected_kwrds
        self.save_to = save_to
        self.force_display_format(display_format)
        self.connect()

    def callback(self, ch, method, properties, body):
        try:
            log = body.decode("utf-8")+"\n"
            if not self.log_display.is_raw_format() and not is_valid_json(log):
                self.log_display.display_format = DisplayFormat.TEXT.name.lower()

            self.log_display.display_logs(log, pattern=self.pattern, keywords=self.selected_keywords,
                                          log_dir=self.save_to)
        except Exception as e:
            self.logger.error(e)


signal(SIGINT, sigint_handler)
