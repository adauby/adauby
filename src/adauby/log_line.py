import logging
import os
import platform
from datetime import date
from rich import print as rprint

from .config import DEFAULT_INDENT

from .utils import PLATFORM_WINDOWS, DisplayFormat, convert_message_to_json, KEY_LABEL, KEY_DATE, KEY_LEVEL, \
    KEYWORDS, TXT_LOG_EXTENSION, JSON_LOG_EXTENSION, MonitoringSummary, JSON_INDENT

CURRENT_DIRECTORY = '.'
FILE_NAME_PREFIX_WINDOWS = '\\adauby_logs'
FILE_NAME_PREFIX_LINUX = '/adauby_logs_'
summary = MonitoringSummary()


class LogWriter:
    def __init__(self, log_dir: str | None = None):
        self.log_dir = log_dir

    def get_file_name(self, file_name_prefix: str = FILE_NAME_PREFIX_LINUX, file_extension=TXT_LOG_EXTENSION):
        today = date.today()
        if self.log_dir == CURRENT_DIRECTORY:
            log_file = os.getcwd()
            log_file = log_file + file_name_prefix + today.strftime('%Y-%m-%d') + file_extension
        else:
            log_file = self.log_dir + file_name_prefix + today.strftime('%Y-%m-%d') + file_extension

        return log_file

    def write_log(self, log_line: str, file_extension=TXT_LOG_EXTENSION, from_file: bool = False):
        if self.log_dir is None or self.log_dir.strip() == '':
            return False
        current_platform: str = platform.system()
        if current_platform == PLATFORM_WINDOWS:
            log_file = self.get_file_name(file_name_prefix=FILE_NAME_PREFIX_WINDOWS,
                                          file_extension=file_extension)
        else:
            log_file = self.get_file_name(file_extension=file_extension)

        with open(log_file, 'a') as dump:
            if not log_line.endswith("\n"):
                log_line = log_line+"\n"
            dump.write(log_line)


class LogDisplay:

    def __init__(self, log_writer: LogWriter | None, display_format: str = DisplayFormat.TEXT.name.lower(),
                 indent=DEFAULT_INDENT):
        self.json_data = None
        self.extension = None
        self.pattern = None
        self.keywords = None
        self.log_writer = log_writer
        self.display_format = display_format
        self._indent = indent
        self.logger = logging.getLogger(f"{__name__}.{self.__class__.__name__}", )

    def get_indent(self):
        if self._indent is None:
            return DEFAULT_INDENT
        return int(self._indent)

    def display_logs(self, log, pattern: str = None, keywords: any = None, log_dir: str = None,
                     from_file: bool = False):
        self.log_writer.log_dir = log_dir
        if keywords is not None and KEYWORDS in keywords:
            keywords = keywords[KEYWORDS]
        summary.increment_total_line_read_count()
        summary.set_keywords(keywords)
        summary.set_pattern(pattern)
        try:
            if self.display_format == DisplayFormat.TEXT.name.lower():
                self.display_line(log_line=log, from_file=from_file, pattern=pattern,
                                  keywords=keywords, extension=TXT_LOG_EXTENSION, json_data=None)
            elif self.display_format == DisplayFormat.JSON.name.lower():
                json_data = convert_message_to_json(log)
                l_line = convert_message_to_json(log)
                self.display_line(log_line=l_line, from_file=from_file, pattern=pattern,
                                  keywords=keywords, extension=JSON_LOG_EXTENSION, json_data=json_data)
            else:
                self.display_line(log_line=log, from_file=from_file, pattern=pattern,
                                  keywords=keywords, extension=TXT_LOG_EXTENSION, json_data=None)
        except Exception as e:
            self.logger.error(f"{type(e)} : {e}")

    def is_raw_format(self):
        return self.display_format == DisplayFormat.TEXT.name.lower()

    def display_line(self, log_line, from_file: bool, **kwargs):
        self.keywords = kwargs[KEYWORDS]
        self.pattern = kwargs["pattern"]
        self.extension = kwargs['extension']
        self.json_data = kwargs['json_data']
        keywords_size = 0
        if self.keywords is not None:
            keywords_size = len(self.keywords)
        try:
            if self.pattern is not None or keywords_size > 0:
                self.display_according_pattern_and_keyword(log_line, from_file)
            else:
                rprint(f"[khaki1]{log_line}[khaki1]")
                self.log_writer.write_log(log_line, file_extension=self.extension,
                                          from_file=from_file)
        except Exception as e:
            print(type(e))
            self.logger.error(e)

    def display_according_pattern_and_keyword(self, log_line, from_file: bool):
        l_line: str = self.get_data_to_display(log_line)
        keyword_found = False
        pattern_found = False
        if self.pattern is not None and self.pattern in log_line:
            l_line = l_line.replace(self.pattern, f"[red]{self.pattern}[khaki1]")
            pattern_found = True

        if self.keywords is not None:
            for keyword in self.keywords:
                if keyword in l_line:
                    keyword_found = True
                    l_line = l_line.replace(keyword, f"[purple]{keyword}[khaki1]")

        if keyword_found and not pattern_found:
            self.display_filtered_line(l_line, log_line, summary.increment_keywords_match, from_file)
        elif pattern_found and not keyword_found:
            self.display_filtered_line(l_line, log_line, summary.increment_pattern_match, from_file)
        elif pattern_found and keyword_found:
            self.display_filtered_line(l_line, log_line,summary.increment_both_keywords_and_pattern_match, from_file)

    def display_filtered_line(self, data_to_show, data_to_save, counter, from_file):
        counter()
        summary.increment_number_of_line_matching_filter()
        rprint(f"[khaki1]{data_to_show}[khaki1]")
        self.log_writer.write_log(f"{data_to_save}", file_extension=self.extension,
                                  from_file=from_file)

    def get_data_to_display(self, log_line):
        if self.json_data is not None:
            return convert_message_to_json(self.json_data, JSON_INDENT)
        else:
            return log_line
