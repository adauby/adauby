import logging
import platform
from configparser import ConfigParser
from uuid import uuid4

from inquirer import Checkbox
from getpass import getpass
from rich import print as rprint
from .utils import DisplayFormat, select_display_format, select_exchange_type, print_message, EMPTY, \
    MUST_BE_A_NUMBER, SLEEP, select_sleep, show_config_param, PLATFORM_WINDOWS

_CONFIG_FILE = "config.ini"
_SECTION_BROKER = "broker"
_SECTION_LOG = "log"
_USER = "user"
_SECRET = "secret"
_HOST = "host"
_WRITE_MODE = "w"
_APPEND_MODE = "a"
_VHOST = "vhost"
_PORT = "port"
KEYWORDS = "keywords"

_QUEUE = 'queue'
_EXCHANGE = 'exchange'
_EXCHANGE_TYPE = 'exchange_type'
DISPLAY_FORMAT = 'display_format'
_INDENT = 'indent'

DEFAULT_HOST = 'localhost'
DEFAULT_USER = 'guest'
DEFAULT_PWD = 'guest'
DEFAULT_VHOST = '/'
DEFAULT_EXCHANGE = "message.direct"
DEFAULT_EXCHANGE_TYPE = 'direct'
DEFAULT_DISPLAY_FORMAT = DisplayFormat.TEXT
DEFAULT_PORT = 5672
DEFAULT_INDENT = 1
DEFAULT_SLEEP = 0.500
FOR_JSON_DISPLAY = "for json display"
FOR_BROKER = "for message broker"
LOG_LEVELS: list = ['INFO', 'WARN', 'ERROR', 'DEBUG']


class Parameters:
    def __init__(self, value_from_user_input, host: str = DEFAULT_HOST, user: str | None = DEFAULT_USER
                 , port: int = DEFAULT_PORT, password: str | None = DEFAULT_PWD, vhost: str | None = DEFAULT_VHOST,
                 keywords: str | None = None, queue_name: str | None = EMPTY, exchange: str | None = DEFAULT_EXCHANGE,
                 exchange_type: str = DEFAULT_EXCHANGE_TYPE,
                 display_format: str | None = DEFAULT_DISPLAY_FORMAT.name.lower(),
                 sleep: float | None = DEFAULT_SLEEP) -> None:

        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.vhost = vhost
        self._keywords = keywords
        self.queue_name = queue_name
        self.exchange = exchange
        self.exchange_type = exchange_type
        self.display_format = display_format
        self._get_numeric_value_from_user_input = value_from_user_input
        self.sleep = sleep

    def _get_field_value(self, new_value, field, default_value):

        if field is None and (new_value is None or new_value.strip() == EMPTY):
            return default_value
        elif field is None and (new_value is not None and new_value.strip() != EMPTY):
            return new_value
        if field is not None and (new_value is None or new_value.strip() == EMPTY):
            return field
        if field is not None and new_value is not None and new_value.strip() != EMPTY:
            return new_value

    def get_keywords(self) -> list:
        if self._keywords is not None and self._keywords.strip() != EMPTY:
            LOG_LEVELS.extend(self._keywords.split(' '))
            return LOG_LEVELS
        return LOG_LEVELS

    def is_labels_empty(self) -> bool:
        return self._keywords is None or self._keywords.strip() == EMPTY

    def get_display_format(self):
        if self.display_format is None:
            return DEFAULT_DISPLAY_FORMAT.name.lower()
        return self.display_format

    def set_user(self, user):
        if user is not None and user.strip() != EMPTY:
            self.user = user
        elif self.user is None or self.user.strip() == EMPTY:
            self.user = DEFAULT_USER

    def set_password(self, password):
        if password is not None and password.strip() != EMPTY:
            self.password = password
        elif self.password is None or self.password.strip() == EMPTY:
            self.password = DEFAULT_PWD

    def set_vhost(self, vhost):
        if vhost is not None and vhost.strip() != EMPTY:
            self.vhost = vhost
        elif self.vhost is None or self.vhost.strip() == EMPTY:
            self.vhost = DEFAULT_VHOST

    def set_host(self, host):
        if host is not None and host.strip() != EMPTY:
            self.host = host
        elif self.host is None or self.host.strip() == EMPTY:
            self.host = DEFAULT_HOST

    def set_port(self, port):
        self.port = self._get_numeric_value_from_user_input(port, self.port, DEFAULT_PORT,
                                                            f"port {FOR_BROKER}")

    def set_exchange(self, exchange):
        self.exchange = self._get_field_value(exchange, self.exchange, DEFAULT_EXCHANGE)

    def set_display_mode(self, display_format):
        self.display_format = self._get_field_value(display_format, self.display_format, DEFAULT_DISPLAY_FORMAT)

    def set_exchange_type(self, exchange_type):
        self.exchange_type = self._get_field_value(exchange_type, self.exchange_type, DEFAULT_EXCHANGE_TYPE)

    def get_sleep(self):
        if self.sleep is None:
            return DEFAULT_SLEEP
        return self.sleep

    def set_sleep(self, sleep):
        self.sleep = float(self._get_field_value(sleep, self.sleep, DEFAULT_SLEEP))

    def get_port(self):
        if self.port is None:
            return self.port
        return str(self.port)


class Configurations:
    def __init__(self, parameters: Parameters, config_file):
        self.logger = logging.getLogger(f"{__name__}.{self.__class__.__name__}", )
        self.parameters = parameters
        self.config = ConfigParser()
        self.file_path = None
        self.config_file = config_file

    def set_parameter(self, section, option, parameter_value):
        if parameter_value is not None:
            self.config[section][option] = parameter_value

    def write_config(self):
        try:
            config_path = self.config_file
            if _SECTION_BROKER not in self.config:
                self.config[_SECTION_BROKER] = {}
            self.set_parameter(_SECTION_BROKER, _HOST, self.parameters.host)
            self.set_parameter(_SECTION_BROKER, _PORT, self.parameters.get_port())
            self.set_parameter(_SECTION_BROKER, _USER, self.parameters.user)
            self.set_parameter(_SECTION_BROKER, _SECRET, self.parameters.password)
            self.set_parameter(_SECTION_BROKER, _VHOST, self.parameters.vhost)
            self.set_parameter(_SECTION_BROKER, _EXCHANGE, self.parameters.exchange)
            self.set_parameter(_SECTION_BROKER, _EXCHANGE_TYPE, self.parameters.exchange_type)
            if self.parameters.queue_name is None or self.parameters.queue_name == "":
                self.parameters.queue_name = "qe.monitoring-" + str(uuid4())
            self.set_parameter(_SECTION_BROKER, _QUEUE, self.parameters.queue_name)
            # TODO: create method for configuring log section only
            if _SECTION_LOG not in self.config:
                self.config[_SECTION_LOG] = {}
            self.set_parameter(_SECTION_LOG, DISPLAY_FORMAT, self.parameters.get_display_format())
            self.set_parameter(_SECTION_LOG, SLEEP, str(self.parameters.get_sleep()))

            with open(config_path, _WRITE_MODE) as config_file:
                self.config.write(config_file)
        except Exception as e:
            self.logger.error(e)


    # TODO: to be refactor (remove method parameter 'labels' and use Parameters class property 'labels' instead)
    def write_keywords(self, keywords: list):
        if _SECTION_LOG not in self.config:
            self.config[_SECTION_LOG] = {}

        if keywords is not None:
            kwrds: str = ''
            for keyword in keywords:
                kwrds += keyword + ' '
            self.config[_SECTION_LOG][KEYWORDS] = kwrds

        with open(_CONFIG_FILE, _WRITE_MODE) as config_file:
            self.config.write(config_file)

    def read_keywords_from_file(self):
        print(f"file_path: {self.file_path}")
        lines = []
        if self.file_path is None or self.file_path.strip() == '':
            raise Exception(f'File path cannot be empty or null!')
        try:
            with open(self.file_path, 'r') as file:
                lines = [line.strip().replace(",", " ") for line in file]
        except FileExistsError as f_err:
            self.logger.error(f_err)
        except FileNotFoundError as f_err:
            self.logger.error(f_err)

        return lines

    def load_keywords(self) -> list:
        if self.config_file is not None:
            self.config.read(self.config_file)
        if _SECTION_LOG in self.config and KEYWORDS in self.config[_SECTION_LOG]:
            kwrds = self.config[_SECTION_LOG][KEYWORDS]
            if kwrds is not None and kwrds.strip() != EMPTY:
                LOG_LEVELS.extend(kwrds.split(' '))

        return LOG_LEVELS

    def build_keywords_selection_prompt(self):
        return [
            Checkbox(
                KEYWORDS,
                message='Select any one label ',
                choices=self.load_keywords()
                , )
        ]

    def set_config_params_from_user_input(self):

        print_message("[green bold]Enter message broker host: [green bold]")
        self.parameters.set_host(input())
        print_message(f"[green bold]Enter message broker port {MUST_BE_A_NUMBER} :")
        self.parameters.set_port(input())
        print_message("[green bold]Enter username: [green bold]")
        self.parameters.set_user(input())
        print_message("[green bold]Enter password: [green bold]")
        self.parameters.set_password(getpass())
        print_message("[green bold]Enter virtual host: [green bold]")
        self.parameters.set_vhost(input())
        print_message("[green bold]Enter exchange name : [green bold]")
        self.parameters.set_exchange(input())
        rprint("[green bold]Select exchange type : [green bold]")
        self.parameters.set_exchange_type(select_exchange_type())
        rprint("[green bold]Select display format : [green bold]")
        self.parameters.set_display_mode(select_display_format(existing_config=self.parameters.display_format))
        rprint("[green bold]Select sleep time between two lines : [green bold]")
        self.parameters.set_sleep(select_sleep(existing_config=self.parameters.sleep))

    def show(self):
        rprint(f"[yellow bold]Broker")
        show_config_param("host", self.parameters.host)
        show_config_param("port", str(self.parameters.port))

        show_config_param("Username", self.parameters.user)
        show_config_param("Password", self.parameters.password)
        show_config_param("vhost", self.parameters.vhost)
        show_config_param("exchange", self.parameters.exchange)
        show_config_param("exchange type", self.parameters.exchange_type)
        show_config_param("queue name", self.parameters.queue_name)
        rprint(f"\n[yellow bold]Log")
        show_config_param("keywords", self.parameters.get_keywords())
        show_config_param("display format", self.parameters.get_display_format())
        show_config_param("sleep time", self.parameters.get_sleep())

    def set_config_param_for_file_only(self):
        rprint("[green bold]Select display format : [green bold]")
        self.parameters.set_display_mode(select_display_format(existing_config=self.parameters.display_format))
        print_message(f"[green bold]Enter indent {FOR_JSON_DISPLAY} {MUST_BE_A_NUMBER} : [green bold]")
        rprint("[green bold]Select sleep time between two lines : [green bold]")
        self.parameters.set_sleep(select_sleep(existing_config=self.parameters.sleep))

# class ConfigurationNotFound(Exception):
#     pass
