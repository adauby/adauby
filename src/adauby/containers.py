import os
import logging.config
import platform

from dependency_injector.containers import DeclarativeContainer
from dependency_injector.providers import Configuration, Singleton, Resource, Factory
from .log_readers import RabbitReader, LogFileReader
from .log_line import LogWriter, LogDisplay
from .config import Configurations, Parameters
from .utils import get_value_from_user_input, PLATFORM_WINDOWS, get_config_path


class Container(DeclarativeContainer):
    current_directory = os.path.dirname(os.path.abspath(__file__))
    if platform.system() == PLATFORM_WINDOWS:
        config_file_dir = current_directory + "\\"
    else:
        config_file_dir = current_directory + "/"
    config_path = get_config_path()+"config.ini"
    config = Configuration(ini_files=[config_path])

    logging = Resource(
        logging.config.fileConfig,
        fname=config_file_dir + 'logging.ini'
    )

    params = Factory(Parameters
                     , get_value_from_user_input
                     , host=config.broker.host
                     , port=config.broker.port
                     , user=config.broker.user
                     , password=config.broker.secret
                     , vhost=config.broker.vhost
                     , keywords=config.log.keywords
                     , queue_name=config.broker.queue
                     , exchange=config.broker.exchange
                     , exchange_type=config.broker.exchange_type
                     , display_format=config.log.display_format
                     , sleep=config.log.sleep)

    log_writer = Singleton(
        LogWriter
    )
    show_log = Singleton(
        LogDisplay
        , log_writer=log_writer
        , indent=config.logs.indent
    )
    message_broker = Singleton(
        RabbitReader
        , log_display=show_log
        , params=params
    )

    log_file_reader = Singleton(
        LogFileReader
        , log_display=show_log
        , params=params
    )

    configuration = Singleton(
        Configurations,
        parameters=params,
        config_file=config_path

    )
