# adauby

## [v0.2.0] - 2024-04-13
### Added
* Display of monitoring summary
* save monitoring summary
* Possibility to continue or stop monitoring when interrupting monitoring (ctrl+c) 
### Fixed
* Issue on filtering by keywords
## [V0.1.0] - 2024-04-03
First release with following features:
* Read log through RabbitMQ
* Read log from file
* filter log according pattern and/or keywords
* display log in different format: json(pretty) or text
* configuration of RabbitMQ client
* configuration of file reader