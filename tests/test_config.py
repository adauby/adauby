from unittest import TestCase, mock
import src.adauby.utils
from src.adauby.config import Parameters, DEFAULT_USER, DEFAULT_PORT, LOG_LEVELS
from src.adauby.utils import DisplayFormat, get_value_from_user_input


class TestParameters(TestCase):

    # def test_is_vhost_invalid_if_vhost_is_empty(self):
    #     assert Parameters(vhost='').is_vhost_invalid()
    #
    # def test_is_vhost_invalid_if_vhost_is_none(self):
    #     assert Parameters(vhost=None).is_vhost_invalid()
    #
    # def test_is_vhost_invalid_if_vhost_is_neither_empty_or_none(self):
    #     assert not Parameters(vhost="/").is_vhost_invalid()

    def test_get_labels_if_labels_is_empty(self):
        assert Parameters(None, keywords='').get_keywords() == LOG_LEVELS

    def test_get_labels_if_labels_is_none(self):
        assert Parameters(None, keywords=None).get_keywords() == LOG_LEVELS

    def test_get_labels_if_labels_is_neither_empty_or_none(self):
        result = Parameters(None, keywords="LABEL_ONE LABEL_TWO").get_keywords()
        assert type(result) is list
        assert len(result) == 6
        assert "LABEL_ONE" in result
        assert "LABEL_TWO" in result
        assert "INFO" in result
        assert "WARN" in result
        assert "DEBUG" in result
        assert "ERROR" in result

    def test_is_labels_empty_if_labels_is_empty(self):
        assert Parameters(None, keywords='').is_labels_empty()

    def test_is_labels_empty_if_labels_is_none(self):
        assert Parameters(None, keywords=None).is_labels_empty()

    def test_is_labels_empty_if_labels_is_neither_empty_nor_none(self):
        assert not Parameters(None, keywords="LABEL_ONE LABEL_TWO").is_labels_empty()

    def test_set_user_if_user_is_empty(self):
        p = Parameters(None)
        p.set_user('')
        assert p.user == DEFAULT_USER

    def test_set_user_if_user_is_none(self):
        p = Parameters(None)
        p.set_user(None)
        assert p.user == DEFAULT_USER

    def test_set_user_if_user_is_empty_with_initialized_empty_user(self):
        p = Parameters(None, user='adauby')
        p.set_user('')
        assert p.user == 'adauby'

    def test_set_user_if_user_is_none_with_initialized_empty_user(self):
        p = Parameters(None, user='adauby')
        p.set_user(None)
        assert p.user == 'adauby'

    def test_set_user_if_user_is_neither_empty_nor_none(self):
        p = Parameters(None)
        p.set_user('new value')
        assert p.user == 'new value'

    def test_get_display_mode_if_display_mode_is_raw(self):
        p = Parameters(None, display_format=DisplayFormat.TEXT.name.lower())
        assert DisplayFormat.TEXT.name.lower() == p.get_display_format()

    # def test_get_display_mode_if_display_mode_is_value(self):
    #     p = Parameters(None, display_format=DisplayFormat.VALUE.name.lower())
    #     assert DisplayFormat.VALUE.name.lower() == p.get_display_format()

    def test_get_display_mode_if_display_mode_is_none(self):
        p = Parameters(None, display_format=None)
        assert DisplayFormat.TEXT.name.lower() == p.get_display_format()

    @mock.patch('src.adauby.utils.input', create=True)
    def test_set_port_if_port_is_empty(self, mocked_input):
        mocked_input.side_effect = ['']
        p = Parameters(get_value_from_user_input, port=8000)

        p.set_port('')
        assert p.port == 8000

    @mock.patch('src.adauby.utils.input', create=True)
    def test_set_port_if_port_is_not_empty_and_not_numeric(self, mocked_input):
        mocked_input.side_effect = ['port']
        p = Parameters(get_value_from_user_input, port=8000)

        p.set_port('wrong')
        assert p.port == 8000

    @mock.patch('src.adauby.utils.input', create=True)
    def test_set_port_if_port_is_numeric(self, mocked_input):
        mocked_input.side_effect = ['port']
        p = Parameters(get_value_from_user_input, port=8000)
        p.set_port('4500')
        assert p.port == 4500

    @mock.patch('src.adauby.utils.input', create=True)
    def test_set_port_if_port_is_not_numeric_first_and_numeric_then(self, mocked_input):
        mocked_input.side_effect = ['6100']
        p = Parameters(get_value_from_user_input, port=8000)
        p.set_port('string')
        assert p.port == 6100

    @mock.patch('src.adauby.utils.input', create=True)
    def test_set_port_if_user_want_to_default_port(self, mocked_input):
        mocked_input.side_effect = ['']
        p = Parameters(get_value_from_user_input)
        p.set_port('')
        assert p.port == DEFAULT_PORT

