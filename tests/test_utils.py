from src.adauby.utils import convert_message_to_json, convert_json_data_to_dictionary, is_valid_json


def test_convert_json_data_to_dictionary_if_message_is_valid():
    json_object = convert_json_data_to_dictionary('{"field":"value"}')
    assert json_object is not None
    assert json_object["field"] == "value"


def test_convert_json_data_to_dictionary_if_message_is_invalid_json_format():
    json_object = convert_json_data_to_dictionary('field:value')
    assert json_object is not None
    assert json_object == "field:value"


def test_convert_message_to_json_if_message_is_valid():
    json_object = convert_message_to_json('{"field":"value"}')
    assert json_object is not None
    assert json_object == '{"field": "value"}'


def test_convert_message_if_message_is_empty():
    json_object = convert_message_to_json('')
    assert json_object == '{}'


def test_convert_message_if_message_is_none():
    json_object = convert_message_to_json(None)
    assert json_object == '{}'


def test_is_valid_json_if_message_is_valid_json():
    assert is_valid_json('{"field": "value"}')


def test_is_valid_json_if_message_is_invalid_json():
    assert not is_valid_json('"field": "invalid"}')


def test_is_valid_json_if_message_is_empty():
    assert not is_valid_json('')
