import pytest
import os
import platform
from datetime import date

from src.adauby.utils import PLATFORM_WINDOWS, KEY_MESSAGE
from src.adauby.log_line import LogWriter, FILE_NAME_PREFIX_LINUX, TXT_LOG_EXTENSION, JSON_LOG_EXTENSION, \
    FILE_NAME_PREFIX_WINDOWS, \
    KEY_DATE, KEY_LEVEL, KEY_LABEL


@pytest.fixture
def current_directory():
    return "."


@pytest.fixture
def test_dir():
    return "test_dir"


@pytest.fixture
def another_directory(test_dir):
    return os.path.join(os.getcwd(), test_dir)


@pytest.fixture
def empty_value():
    return ''


@pytest.fixture
def file_path():
    today = date.today()
    fp = os.path.join(os.getcwd(), os.getcwd() + FILE_NAME_PREFIX_LINUX + today.strftime(
        '%Y-%m-%d') + TXT_LOG_EXTENSION)
    yield fp

    os.remove(fp)


class TestLogWriter:
    @pytest.mark.integration_test
    def test_get_file_name_if_log_dir_is_current_directory(self, current_directory):
        file_name = LogWriter(current_directory).get_file_name()
        today = date.today()
        current_platform: str = platform.system()
        if current_platform == PLATFORM_WINDOWS:
            assert file_name == os.getcwd() + FILE_NAME_PREFIX_WINDOWS + today.strftime('%Y-%m-%d') + TXT_LOG_EXTENSION
        else:
            assert file_name == os.getcwd() + FILE_NAME_PREFIX_LINUX + today.strftime('%Y-%m-%d') + TXT_LOG_EXTENSION

    @pytest.mark.integration_test
    def test_get_file_name_if_log_dir_is_not_current_directory(self, another_directory, test_dir):
        file_name = LogWriter(another_directory).get_file_name()
        today = date.today()
        current_platform: str = platform.system()
        if current_platform == PLATFORM_WINDOWS:
            assert file_name == os.path.join(os.getcwd(), test_dir) + FILE_NAME_PREFIX_WINDOWS + today.strftime(
                '%Y-%m-%d') + TXT_LOG_EXTENSION
        else:
            assert file_name == os.path.join(os.getcwd(), test_dir) + FILE_NAME_PREFIX_LINUX + today.strftime(
                '%Y-%m-%d') + TXT_LOG_EXTENSION

    @pytest.mark.integration_test
    def test_get_file_name_if_file_extension_is_json(self, current_directory):
        file_name = LogWriter(current_directory).get_file_name(file_extension=JSON_LOG_EXTENSION)
        assert file_name.endswith(JSON_LOG_EXTENSION)

    @pytest.mark.integration_test
    def test_write_log_if_log_dir_is_empty_value(self, empty_value):
        result = LogWriter(empty_value).write_log(
            f"{KEY_DATE}:'2024-03-14 05:01:00', {KEY_LEVEL}:'INFO', {KEY_LABEL}:'TEST', {KEY_MESSAGE}:'log message'")
        assert result is False

    @pytest.mark.integration_test
    def test_write_log_if_log_dir_is_none(self):
        result = LogWriter(None).write_log(
            f"{KEY_DATE}:'2024-03-14 05:01:00', {KEY_LEVEL}:'INFO', {KEY_LABEL}:'TEST', {KEY_MESSAGE}:'log message'")
        assert result is False

    @pytest.mark.integration_test
    def test_write_log_if_log_dir_is_current_directory(self, current_directory, file_path):
        LogWriter(current_directory).write_log(
            f"{KEY_DATE}:'2024-03-14 05:01:00', {KEY_LEVEL}:'INFO', {KEY_LABEL}:'TEST', {KEY_MESSAGE}:'log message'")

        exists = os.path.exists(file_path)
        assert exists is True
        with open(file_path, 'r') as log_file:
            line = log_file.readline()
            assert line == "date:'2024-03-14 05:01:00', level:'INFO', label:'TEST', message:'log message'\n"
