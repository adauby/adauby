from src.adauby.log_line import LogDisplay
from src.adauby.utils import DisplayFormat


class TestLogDisplay:

    def test_is_raw_format_if_display_format_is_raw(self):
        ld = LogDisplay(None, display_format=DisplayFormat.TEXT.name.lower())
        assert ld.is_raw_format()

    # def test_is_raw_format_if_display_format_is_value(self):
    #     ld = LogDisplay(None, display_format=DisplayFormat.VALUE.name.lower())
    #     assert not ld.is_raw_format()
    #
    # def test_is_raw_format_if_display_format_is_json(self):
    #     ld = LogDisplay(None, display_format=DisplayFormat.JSON.name.lower())
    #     assert not ld.is_raw_format()
