import pytest
import configparser

from src.adauby.config import Configurations, Parameters, LOG_LEVELS


@pytest.fixture()
def config_parser():
    return configparser.ConfigParser()


class TestConfigurations:
    @pytest.mark.integration_test
    def test_load_labels_if_log_section_does_not_exists_in_config_file(self, config_parser):
        keywords = Configurations(Parameters(None), None).load_keywords()
        assert LOG_LEVELS == keywords
        assert 'INFO' in keywords
        assert 'WARN' in keywords
        assert 'DEBUG' in keywords
        assert 'ERROR' in keywords
