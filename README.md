# Adauby
## Overview
A Command-line tool for:
* monitoring log
* filtering log
* saving log

It supports:
* RabbitMQ:
  * topic exchange
  * direct exchange
  * fanout exchange
* Text file
* Json file

## Documentation
### Getting started
Read a log file by executing this:
```shell
adauby monitoring --read-file /path/to/file.log
```

Save result of looking for a pattern by doing this:
```shell
adauby monitoring -p word --save-to-dir /path/to/dorectory
```
### User guide 
[See here](user_guide.md)

## Install
you need python >=3.10 and <3.12 (may work with 3.7 3.8 and 3.9 but not tested yet).
>*NB: For windows you also need:
> * c++ 14 or higher
> * to add the python scripts folder to PATH

```shell
pip install adauby --index-url https://gitlab.com/api/v4/projects/54123282/packages/pypi/simple
```
## Uninstall
```shell
pip uninstall adauby
```

## Licence
This project is under the MIT license.
