## Usage
Use ```--help``` option to see more details about commands
```shell
adauby --help
```
You can also ```--help``` option with a command to see more details about its options <br/>
Example: ```adauby config --help``` will show details about ***config*** options.

### Commands

```shell
config          Allow to set configuration parameters
monitoring     Monitor log for message broker or file
```

### Options
```shell
--install-completion          Install completion for the current shell.                                                                    
--show-completion             # Show completion for the current shell, to copy it or customize the 										  installation.                                  
--help                        Show this message and exit.  
```
## Configuration
You need to configure the tool before using it.
The command above will allow you to set parameters of the message broker client and the file reader:
```shell
adauby config
```

If you want to read only file add the option ```--file-only``` or ```-f```.
```shell
adauby config --file-only
```

The show the current configuration use the command below:
```shell
adauby config --show
```
### Options
```shell
-k, --set-keywords    Specify file path containing keywords.
-S, --show            Show actual configuration                                                                   
-f, --file-only       Set configuration for reading file only                                                                            
--help                Show this message and exit. 
```
### Example
```shell
adauby config --set-keywords  /path/to/keywords.txt # will allow to configure the message broker client and 													# the file read with keywords 
                                                    # and load keywords from file /path/to/keywords.txt for 													# the configuration 
```
```--set-keywords``` load keywords from a file. keywords must be separated by ```,``` or space. Keywords can be used to filter log.
## Monitoring
```adauby monitor``` is used monitor log. This command will try to connect to a message broker (rabbitMQ by default). It will fail if it's not configure or if the message broker is unreachable.
To read a file add the option ```--read-file```.

### Options
```shell
-p, --pattern               Pattern to search in logs                              
-s, --save-to-dir           Directory where to save logs. To save in the current directory use ".".                                                                                                                                          │
-k, --filter-by-keywords    Filter logs by key word. The default keywords are: INFO, WARN, ERROR, DEBUG.                                  
-d, --force-display         Force display format: [TEXT (default), JSON.]                                                             
-f, --read-file             Read log from file.      
-F, --force-sleep           Force sleep time between two lines [1 second, 0.7 second - 0.5 second (default), 0.3 second, 0.1 second].                                                                                   
--help                      Show this message and exit. 
```
> **NB**:<br/>
> * ```--read-file``` and ```--save-to-dir``` will work together only if you filter by pattern (```--pattern``` or ```-p```) or/and by keywords (```----filter-by-keywords``` or ```-k```)
> * when ```--save-to-dir``` is used, log is saved in a file. The name of each log file use the pattern ***adauby_logs_yyyy-mm-dd.log*** or ***adauby_logs_yyyy-mm-dd.json*** (*if you use json display format*) as pattern for the file name.
>   * **yyyy** : year
>   * **mm** :  month
>   * **dd** : day

### Examples
```shell
adauby monitoring -p search # will display lines containing the word *search*

adauby monitoring --save-to-dir /path/to/my_directory # will display and save log to /path/to/my_directory/adauby_logs_yyyy-mm-dd.log

adauby monitoring --force-display # will allow you to force display format of log

adauby monitoring -k # will allow to select some keywords and display the only the line containing the selected keywords 
```
## Monitoring summary
When you choose to end the monitoring, a summary of the monitoring is display. it looks like this:
```json
{
  "from": "FILE",
  "file name": "/path/to/file_read.log",
  "started at": "2024-04-13 16:30:59",
  "ended at": "2024-04-13 16:31:13",
  "total lines read": 18,
  "filter": {
    "pattern": "word",
    "keywords": [
      "INFO",
      "ERROR"
    ],
    "pattern-only match": 0,
    "keyword-only match": 18,
    "matching both keywords and pattern": 0,
    "total filtered": 18,
    "missed": 0
  }
}
```
The summary is saved in a json file called ```summaries_yyyy-MM-dd.json``` where '**yyyy**' is for year, '**mm**' is for  month and '**dd**' is for day.
The file is located in ```/{USER_HOME}/adauby/logs/summaries``` for linux or ```c:\{USER_HOME}\adauby\logs\summaries``` for windows.
Summaries are saved by day. All summaries generated in a same day will be saved in the same file.

